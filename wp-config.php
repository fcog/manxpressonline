<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'manxpressdb');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'dbmanexpress');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'H6%sg$shgTs4');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', ',z`hCC33jB(EYvasn|mdp7TTAehW|S[}]tc+>M!j+cb-}SWTZrwuN9@<lbG,)k- '); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', '7:{-RhSz|=%e~Fk_^X_qcWW_5fXkAeq.3$bf=yo;4M>HKXaQQavD|Efb;1yiJ8b+'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', 'm|Al.K?WnjM}[Phv&o@.@_w9rm$}+Q)+dXJ0|HJf`--hr5<9/.$oCT/*:EMwYEr]'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', '~[w8zx|KR65(kN$rG#e{l2(W&=qc~Q5(>HNZ2!AB`wI&-(|Mfv*Sr`7<JmYi{T3n'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', 'koARt~+2#K{s$|+>SxLFJlf,hxU!q<p:@Mfk?M&r-Z*~|$S^-[NAjTYV4xUj*kuk'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', 's`F|l4,sUlGW|ZP-rI8z:W;Tbna0O6VO)2?tdWrRY^CAXv;8:=&n$O;K-7I>}q7<'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', '.;:|e;K>hRiNI%BviEK)eZ.y>#2y|tF^v~XW*;|kW;K7cUjqJ|kU*(I&L&,I+[k;'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', ' OjT4,9dH:Q04?Kfyt6F2OO`t9w0ev/(:8keG,Ed pp:6W5Xdk+6^6SsgdHF|s.z'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catalán.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

